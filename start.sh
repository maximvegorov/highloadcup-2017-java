#!/usr/bin/env bash

set -e

java -server \
    -showversion \
    -cp "app.jar:$(echo $(find libs -type f -name '*.jar' | sort) | sed -r 's/\s+/:/g')" \
    -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+UseCGroupMemoryLimitForHeap \
    -XX:+UseParallelGC \
    -XX:+UseParallelOldGC \
    -XX:MaxGCPauseMillis=100 \
    -XX:-OmitStackTraceInFastThrow \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=1099 \
    -Dcom.sun.management.jmxremote.local.only=false \
    -Dcom.sun.management.jmxremote.rmi.port=1099 \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Djava.rmi.server.hostname=localhost \
    -Dfile.encoding=UTF-8 \
    -Djava.awt.headless=true \
    -Djava.security.egd=file:/dev/./urandom \
    -Dnetworkaddress.cache.ttl=60 \
    -Dsun.net.client.defaultConnectTimeout=10000 \
    -Dsun.net.client.defaultReadTimeout=10000 \
    -Duser.language=en \
    -Duser.country=US \
    -Duser.timezone=GMT \
    -Dnetty.server.port=80 \
    ru.maximvegorov.highloadcup.Application
