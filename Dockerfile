FROM openjdk:8-jdk-slim

COPY start.sh /app/app.sh
COPY build/libs/dependencies/* /app/libs/
COPY build/libs/highloadcup-2017-java-LATEST.jar /app/app.jar

EXPOSE 80 5005 1099

WORKDIR /app
ENTRYPOINT ["/app/app.sh"]
