#!/usr/bin/env bash

set -e

docker login -u "$1" -p "$2" stor.highloadcup.ru
docker tag "$3" stor.highloadcup.ru/travels/strong_fossa
docker push stor.highloadcup.ru/travels/strong_fossa
