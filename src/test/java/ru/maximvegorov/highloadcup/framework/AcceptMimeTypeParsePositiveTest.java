package ru.maximvegorov.highloadcup.framework;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public final class AcceptMimeTypeParsePositiveTest {
    @Parameterized.Parameter
    public String mimeTypeLine;

    @Parameterized.Parameter(value = 1)
    public AcceptMimeType expected;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> getParameters() {
        return Arrays.asList(
                new Object[]{"*/*", new AcceptMimeType("*", "*", 10)},
                new Object[]{"*/*;q=2.0", new AcceptMimeType("*", "*", 2000)},
                new Object[]{"type/*", new AcceptMimeType("type", "*", 20)},
                new Object[]{"type/*;q=2.0", new AcceptMimeType("type", "*", 2000)},
                new Object[]{"type/subtype", new AcceptMimeType("type", "subtype", 1000)},
                new Object[]{"type/subtype;q=2.0", new AcceptMimeType("type", "subtype", 2000)});
    }


    @Test
    public void testParse() throws Exception {
        assertThat(AcceptMimeType.parse(mimeTypeLine)).isEqualTo(expected);
    }
}
