package ru.maximvegorov.highloadcup.framework;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class SimplePathMatcherTest {
    @Parameterized.Parameter
    public String pathPattern;
    @Parameterized.Parameter(value = 1)
    public String path;
    @Parameterized.Parameter(value = 2)
    public Map<String, String> expectedResult;

    private SimplePathMatcher pathMatcher;

    @Parameterized.Parameters(name = "{0}-{1}")
    public static Collection<Object[]> getParameters() {
        return Arrays.asList(
                new Object[]{"/", "/", emptyMap()},
                new Object[]{"/", "//", null},
                new Object[]{"/a", "/", null},
                new Object[]{"/a", "/a", emptyMap()},
                new Object[]{"/a/b", "/a", null},
                new Object[]{"/a/b", "/a/b", emptyMap()},
                new Object[]{"/a/b", "/a/b/", null},
                new Object[]{"/a/b", "/a/b/c", null},
                new Object[]{"/a/b", "/a/b", emptyMap()},
                new Object[]{"/a/:param/c", "/a/b/c", singletonMap("param", "b")},
                new Object[]{"/a/:param1/:param2", "/a/b/c", ImmutableMap.of("param1", "b", "param2", "c")});
    }

    @Test
    public void test() {
        PathMatcher pathMatcher = new SimplePathMatcher(pathPattern);

        assertThat(pathMatcher.matches(path)).isEqualTo(expectedResult);
    }
}
