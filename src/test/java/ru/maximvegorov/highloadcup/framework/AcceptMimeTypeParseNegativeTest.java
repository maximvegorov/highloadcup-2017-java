package ru.maximvegorov.highloadcup.framework;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public final class AcceptMimeTypeParseNegativeTest {
    @Parameterized.Parameter
    public String mimeTypeLine;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<String> getParameters() {
        return Arrays.asList(
                "type",
                "type/subtype;",
                "type/subtype;a",
                "type/subtype;q=",
                "type/subtype;q=1",
                "type/subtype;q=1.",
                "type/subtype;q=1.0 ");
    }

    @Test(expected = AcceptMimeType.CantParseMimeTypeLine.class)
    public void test() {
        AcceptMimeType.parse(mimeTypeLine);
    }
}
