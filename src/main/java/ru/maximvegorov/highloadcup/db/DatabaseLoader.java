package ru.maximvegorov.highloadcup.db;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.val;
import ru.maximvegorov.highloadcup.db.rows.LocationRow;
import ru.maximvegorov.highloadcup.db.rows.UserRow;
import ru.maximvegorov.highloadcup.db.rows.VisitRow;

public final class DatabaseLoader {
    private final ObjectMapper mapper;

    public DatabaseLoader() {
        mapper = new ObjectMapper();
        mapper.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        mapper.disable(DeserializationFeature.ACCEPT_FLOAT_AS_INT);
        mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public void loadData(DatabaseService database, Path dataZipFileName) {
        try (val dataZipFile = new ZipInputStream(
                new BufferedInputStream(
                        Files.newInputStream(dataZipFileName, StandardOpenOption.READ))))
        {
            for (ZipEntry zipEntry = dataZipFile.getNextEntry();
                    zipEntry != null; zipEntry = dataZipFile.getNextEntry()) {
                if (zipEntry.getName().startsWith("users_")) {
                    database.getUserTable().appendRows(
                            mapper.readValue(dataZipFile, UsersWrapper.class).getUsers());
                } else if (zipEntry.getName().startsWith("locations_")) {
                    database.getLocationTable().appendRows(
                            mapper.readValue(dataZipFile, LocationsWrapper.class).getLocations());
                } else if (zipEntry.getName().startsWith("visits_")) {
                    database.getVisitTable().appendRows(
                            mapper.readValue(dataZipFile, VisitsWrapper.class).getVisits());
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Data
    private static final class UsersWrapper {
        private List<UserRow> users;
    }

    @Data
    private static final class LocationsWrapper {
        private List<LocationRow> locations;
    }

    @Data
    private static final class VisitsWrapper {
        private List<VisitRow> visits;
    }
}
