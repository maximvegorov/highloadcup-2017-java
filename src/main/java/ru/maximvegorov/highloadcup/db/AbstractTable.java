package ru.maximvegorov.highloadcup.db;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.ParametersAreNonnullByDefault;

import lombok.Getter;
import ru.maximvegorov.highloadcup.db.rows.EntityWithIdRow;

@ParametersAreNonnullByDefault
public abstract class AbstractTable<Row extends EntityWithIdRow> {
    private final ReadWriteLock tableLock = new ReentrantReadWriteLock();
    @Getter
    private final Map<Integer, Row> primaryKey = new HashMap<>();

    public AcquiredLock acquireReadLock() {
        return new AcquiredLock(tableLock.readLock());
    }

    public AcquiredLock acquireWriteLock() {
        return new AcquiredLock(tableLock.writeLock());
    }

    public final void appendRows(Collection<Row> rows) {
        for (Row row : rows) {
            primaryKey.put(row.getId(), row);
            afterInsertRow(row);
        }
    }

    public final boolean contains(Integer id) {
        try (AcquiredLock readLock = acquireReadLock()) {
            return primaryKey.containsKey(id);
        }
    }

    public final <T> Optional<T> getById(Integer id, Function<Row, T> mapper) {
        try (AcquiredLock readLock = acquireReadLock()) {
            return Optional.ofNullable(primaryKey.get(id))
                    .map(mapper);
        }
    }

    protected void afterInsertRow(Row row) {
    }

    public final boolean insert(Integer id, Supplier<Row> creator) {
        try (AcquiredLock writeLock = acquireWriteLock()) {
            if (primaryKey.containsKey(id)) {
                return false;
            }
            Row newRow = creator.get();
            primaryKey.put(id, newRow);
            afterInsertRow(newRow);
            return true;
        }
    }

    protected void beforeUpdateRow(Row row) {
    }

    protected void afterUpdateRow(Row row) {
    }

    public final boolean update(Integer id, Consumer<Row> updater) {
        try (AcquiredLock writeLock = acquireWriteLock()) {
            Row row = primaryKey.get(id);
            if (row == null) {
                return false;
            }

            beforeUpdateRow(row);
            updater.accept(row);
            afterUpdateRow(row);

            return true;
        }
    }
}
