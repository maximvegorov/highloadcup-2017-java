package ru.maximvegorov.highloadcup.db;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;
import java.util.stream.Stream;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import lombok.Getter;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import ru.maximvegorov.highloadcup.common.types.Gender;
import ru.maximvegorov.highloadcup.db.rows.LocationRow;
import ru.maximvegorov.highloadcup.db.rows.UserRow;
import ru.maximvegorov.highloadcup.db.rows.UserVisitRow;
import ru.maximvegorov.highloadcup.db.rows.VisitRow;
import ru.maximvegorov.highloadcup.db.tables.LocationTable;
import ru.maximvegorov.highloadcup.db.tables.UserTable;
import ru.maximvegorov.highloadcup.db.tables.VisitTable;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Getter
@ParametersAreNonnullByDefault
public final class DatabaseService {
    private final UserTable userTable = new UserTable();
    private final LocationTable locationTable = new LocationTable();
    private final VisitTable visitTable = new VisitTable();

    private static Stream<Integer> getVisitIdsByDateRange(
            NavigableMap<Long, Set<Integer>> visitIndexRow, @Nullable Long fromDate, @Nullable Long toDate)
    {
        NavigableMap<Long, Set<Integer>> filteredMap;
        if (fromDate != null && toDate != null) {
            if (fromDate > toDate) {
                return Stream.empty();
            }
            filteredMap = visitIndexRow.subMap(fromDate, false, toDate, false);
        } else if (fromDate != null) {
            filteredMap = visitIndexRow.tailMap(fromDate, false);
        } else if (toDate != null) {
            filteredMap = visitIndexRow.headMap(toDate, false);
        } else {
            filteredMap = visitIndexRow;
        }
        return filteredMap.values().stream().flatMap(Collection::stream);
    }

    public List<UserVisitRow> getUserVisits(
            Integer userId,
            @Nullable Long fromDate,
            @Nullable Long toDate,
            @Nullable String country,
            @Nullable Integer toDistance)
    {
        try (AcquiredLock visitReadLock = visitTable.acquireReadLock()) {
            val visitUserIdIndexRow = visitTable.getUserIdIndexRow(userId);
            if (visitUserIdIndexRow.isEmpty()) {
                return emptyList();
            }

            try (AcquiredLock locationReadLock = locationTable.acquireReadLock()) {
                Stream<Pair<VisitRow, LocationRow>> result =
                        getVisitIdsByDateRange(visitUserIdIndexRow, fromDate, toDate)
                                .map(visitTable.getPrimaryKey()::get)
                                .map(v -> Pair.of(v, locationTable.getPrimaryKey().get(v.getLocationId())));

                if (country != null) {
                    result = result.filter(p -> p.getRight().getCountry().equals(country));
                }

                if (toDistance != null) {
                    result = result.filter(p -> p.getRight().getDistance() < toDistance);
                }

                return result.map(p -> new UserVisitRow(p.getLeft().getMark(), p.getLeft().getVisitedAt(),
                        p.getRight().getPlace()))
                        .sorted(Comparator.comparingLong(UserVisitRow::getVisitedAt))
                        .collect(toList());
            }
        }
    }

    public double getLocationAvgMark(
            Integer locationId,
            @Nullable Long fromDate,
            @Nullable Long toDate,
            @Nullable Long fromAge,
            @Nullable Long toAge,
            @Nullable Gender gender,
            int precision)
    {
        try (AcquiredLock visitReadLock = visitTable.acquireReadLock()) {
            NavigableMap<Long, Set<Integer>> locationIdIndexRow = visitTable.getLocationIdIndexRow(locationId);
            if (locationIdIndexRow.isEmpty()) {
                return 0;
            }

            try (AcquiredLock userReadLock = userTable.acquireReadLock()) {
                Stream<Pair<VisitRow, UserRow>> result = getVisitIdsByDateRange(locationIdIndexRow, fromDate, toDate)
                        .map(visitTable.getPrimaryKey()::get)
                        .map(v -> Pair.of(v, userTable.getPrimaryKey().get(v.getUserId())));

                val now = Instant.now().atOffset(ZoneOffset.UTC);

                if (fromAge != null) {
                    val fromBirthdate = now.minusYears(fromAge).toInstant().getEpochSecond();
                    result = result.filter(p -> p.getRight().getBirthdate() < fromBirthdate);
                }

                if (toAge != null) {
                    val toBirthdate = now.minusYears(toAge).toInstant().getEpochSecond();
                    result = result.filter(p -> p.getRight().getBirthdate() > toBirthdate);
                }

                if (gender != null) {
                    result = result.filter(p -> p.getRight().getGender() == gender);
                }

                val average = result.mapToDouble(p -> p.getLeft().getMark())
                        .average()
                        .orElse(0);

                return (((long) (10 * precision * average) + 5) / 10) / (double) precision;
            }
        }
    }
}
