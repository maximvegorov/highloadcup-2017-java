package ru.maximvegorov.highloadcup.db.rows;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.maximvegorov.highloadcup.common.types.Gender;

@Data
public final class UserRow implements EntityWithIdRow {
    public static final int MAX_EMAIL_SIZE = 100;
    public static final int MAX_NAME_SIZE = 50;

    private Integer id;
    private String email;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private Gender gender;
    @JsonProperty("birth_date")
    private long birthdate;
}
