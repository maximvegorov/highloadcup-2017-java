package ru.maximvegorov.highloadcup.db.rows;

import lombok.Data;

@Data
public final class LocationRow implements EntityWithIdRow {
    public static final int MAX_COUNTRY_SIZE = 50;
    public static final int MAX_CITY_SIZE = 50;

    private Integer id;
    private String place;
    private String country;
    private String city;
    private int distance;
}
