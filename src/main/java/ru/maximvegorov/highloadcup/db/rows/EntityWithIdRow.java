package ru.maximvegorov.highloadcup.db.rows;

public interface EntityWithIdRow {
    Integer getId();
}
