package ru.maximvegorov.highloadcup.db.rows;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserVisitRow {
    private byte mark;
    private long visitedAt;
    private String place;
}
