package ru.maximvegorov.highloadcup.db.rows;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class VisitRow implements EntityWithIdRow {
    private Integer id;
    @JsonProperty("location")
    private Integer locationId;
    @JsonProperty("user")
    private Integer userId;
    @JsonProperty("visited_at")
    private Long visitedAt;
    private byte mark;
}
