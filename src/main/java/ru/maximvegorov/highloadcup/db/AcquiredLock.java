package ru.maximvegorov.highloadcup.db;

import java.util.concurrent.locks.Lock;

public final class AcquiredLock implements AutoCloseable {
    private final Lock lock;


    AcquiredLock(Lock lock) {
        this.lock = lock;
        lock.lock();
    }

    @Override
    public void close() {
        lock.unlock();
    }
}
