package ru.maximvegorov.highloadcup.db.tables;

import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.rows.UserRow;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public final class UserTable extends AbstractTable<UserRow> {
}
