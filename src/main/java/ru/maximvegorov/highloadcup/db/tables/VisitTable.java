package ru.maximvegorov.highloadcup.db.tables;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.rows.VisitRow;

import static java.util.Collections.emptyNavigableMap;

@ParametersAreNonnullByDefault
public final class VisitTable extends AbstractTable<VisitRow> {
    private final Map<Integer, NavigableMap<Long, Set<Integer>>> visitByUserIdIndex = new HashMap<>();
    private final Map<Integer, NavigableMap<Long, Set<Integer>>> visitByLocationIdIndex = new HashMap<>();

    private static void removeVisitedAtAndVisitIdPair(
            NavigableMap<Long, Set<Integer>> map, Long visitedAt, Integer visitId)
    {
        Set<Integer> visitIds = map.get(visitedAt);
        if (visitIds != null) {
            visitIds.remove(visitId);
            if (visitIds.isEmpty()) {
                map.remove(visitedAt);
            }
        }
    }

    @Override
    protected void afterInsertRow(VisitRow visitRow) {
        visitByUserIdIndex.computeIfAbsent(visitRow.getUserId(), userId -> Maps.newTreeMap())
                .computeIfAbsent(visitRow.getVisitedAt(), visitedAt -> Sets.newHashSet())
                .add(visitRow.getId());
        visitByLocationIdIndex.computeIfAbsent(visitRow.getLocationId(), locationId -> Maps.newTreeMap())
                .computeIfAbsent(visitRow.getVisitedAt(), visitedAt -> Sets.newHashSet())
                .add(visitRow.getId());
    }

    @Override
    protected void beforeUpdateRow(VisitRow visitRow) {
        Optional.ofNullable(visitByUserIdIndex.get(visitRow.getUserId()))
                .ifPresent(m -> removeVisitedAtAndVisitIdPair(m, visitRow.getVisitedAt(), visitRow.getId()));
        Optional.ofNullable(visitByLocationIdIndex.get(visitRow.getLocationId()))
                .ifPresent(m -> removeVisitedAtAndVisitIdPair(m, visitRow.getVisitedAt(), visitRow.getId()));
    }

    @Override
    protected void afterUpdateRow(VisitRow visitRow) {
        afterInsertRow(visitRow);
    }

    public NavigableMap<Long, Set<Integer>> getUserIdIndexRow(Integer userId) {
        return visitByUserIdIndex.getOrDefault(userId, emptyNavigableMap());
    }

    public NavigableMap<Long, Set<Integer>> getLocationIdIndexRow(Integer locationId) {
        return visitByLocationIdIndex.getOrDefault(locationId, emptyNavigableMap());
    }
}
