package ru.maximvegorov.highloadcup.framework;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
final class AcceptMimeType {
    private final String type;
    private final String subType;
    private final int q;

    AcceptMimeType(String type, String subType, int q) {
        this.type = type;
        this.subType = subType;
        if (q < 0) {
            if (type.equals("*")) {
                q = 10;
            } else if (subType.equals("*")) {
                q = 20;
            } else {
                q = 1000;
            }
        }
        this.q = q;
    }

    private static boolean isDecDigit(int c) {
        return '0' <= c && c <= '9';
    }

    private static int parseQuality(String mimeTypeLine, int startIndex) {
        if (!mimeTypeLine.startsWith("q=", startIndex)) {
            throw new CantParseMimeTypeLine();
        }
        startIndex += 2;

        if (startIndex >= mimeTypeLine.length() || !isDecDigit(mimeTypeLine.charAt(startIndex))) {
            throw new CantParseMimeTypeLine();
        }

        double result = 0;
        while (startIndex < mimeTypeLine.length() && isDecDigit(mimeTypeLine.charAt(startIndex))) {
            result = 10 * result + (mimeTypeLine.charAt(startIndex) - '0');
            startIndex++;
        }

        if (startIndex >= mimeTypeLine.length() || mimeTypeLine.charAt(startIndex) != '.') {
            throw new CantParseMimeTypeLine();
        }
        startIndex++;

        if (startIndex >= mimeTypeLine.length() || !isDecDigit(mimeTypeLine.charAt(startIndex))) {
            throw new CantParseMimeTypeLine();
        }

        double factor = 0.1;
        while (startIndex < mimeTypeLine.length() && isDecDigit(mimeTypeLine.charAt(startIndex))) {
            result += factor * (mimeTypeLine.charAt(startIndex) - '0');
            factor /= 10;
            startIndex++;
        }

        if (startIndex < mimeTypeLine.length()) {
            throw new CantParseMimeTypeLine();
        }

        return (int) (1000 * result);
    }

    static AcceptMimeType parse(String mimeTypeLine) {
        int endOfType = mimeTypeLine.indexOf('/');
        if (endOfType == -1 || endOfType == 0) {
            throw new CantParseMimeTypeLine();
        }

        int startOfSubType = endOfType + 1;
        int endOfSubType = mimeTypeLine.indexOf(';', startOfSubType);
        if (endOfSubType == -1) {
            endOfSubType = mimeTypeLine.length();
        }

        int q = -1;
        if (endOfSubType < mimeTypeLine.length()) {
            q = parseQuality(mimeTypeLine, endOfSubType + 1);
        }

        return new AcceptMimeType(
                mimeTypeLine.substring(0, endOfType),
                mimeTypeLine.substring(startOfSubType, endOfSubType),
                q);
    }

    @Override
    public String toString() {
        return "AcceptMimeType{" +
                "type='" + type + '\'' +
                ", subType='" + subType + '\'' +
                ", q=" + q +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AcceptMimeType that = (AcceptMimeType) o;

        if (q != that.q) {
            return false;
        }
        if (!type.equals(that.type)) {
            return false;
        }
        return subType.equals(that.subType);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + subType.hashCode();
        result = 31 * result + q;
        return result;
    }

    String getType() {
        return type;
    }

    String getSubType() {
        return subType;
    }

    int getQ() {
        return q;
    }

    static final class CantParseMimeTypeLine extends RuntimeException {
    }
}
