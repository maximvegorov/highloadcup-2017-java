package ru.maximvegorov.highloadcup.framework.exceptions;

import io.netty.handler.codec.http.HttpResponseStatus;

public class UnsupportedMediaTypeException extends HttpStatusErrorException {
    public UnsupportedMediaTypeException() {
        super(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public UnsupportedMediaTypeException(Throwable cause) {
        super(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE, cause);
    }
}
