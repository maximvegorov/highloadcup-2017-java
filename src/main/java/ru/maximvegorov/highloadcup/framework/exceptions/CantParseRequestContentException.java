package ru.maximvegorov.highloadcup.framework.exceptions;

public final class CantParseRequestContentException extends RuntimeException {
    public CantParseRequestContentException(Throwable cause) {
        super(cause);
    }
}
