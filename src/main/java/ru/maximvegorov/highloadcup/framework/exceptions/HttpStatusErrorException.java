package ru.maximvegorov.highloadcup.framework.exceptions;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.handler.codec.http.HttpResponseStatus;

@ParametersAreNonnullByDefault
public abstract class HttpStatusErrorException extends RuntimeException {
    private final HttpResponseStatus status;

    HttpStatusErrorException(HttpResponseStatus status) {
        this(status, null);
    }

    HttpStatusErrorException(HttpResponseStatus status, @Nullable Throwable cause) {
        super("Http error: " + status, cause);
        this.status = status;
    }

    public HttpResponseStatus getStatus() {
        return status;
    }
}
