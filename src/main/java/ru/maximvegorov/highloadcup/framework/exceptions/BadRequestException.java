package ru.maximvegorov.highloadcup.framework.exceptions;

import io.netty.handler.codec.http.HttpResponseStatus;

public final class BadRequestException extends HttpStatusErrorException {
    public BadRequestException(Throwable cause) {
        super(HttpResponseStatus.BAD_REQUEST, cause);
    }
}
