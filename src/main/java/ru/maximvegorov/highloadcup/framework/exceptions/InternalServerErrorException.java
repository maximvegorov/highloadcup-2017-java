package ru.maximvegorov.highloadcup.framework.exceptions;

import io.netty.handler.codec.http.HttpResponseStatus;

public final class InternalServerErrorException extends HttpStatusErrorException {
    public InternalServerErrorException() {
        super(HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }
}
