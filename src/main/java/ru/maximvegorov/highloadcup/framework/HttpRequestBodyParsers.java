package ru.maximvegorov.highloadcup.framework;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpUtil;
import ru.maximvegorov.highloadcup.framework.exceptions.UnsupportedMediaTypeException;
import ru.maximvegorov.highloadcup.framework.parsers.JsonBodyParser;
import ru.maximvegorov.highloadcup.framework.parsers.PlainTextBodyParser;

@ParametersAreNonnullByDefault
public final class HttpRequestBodyParsers {
    private static final HttpRequestBodyParser DEFAULT = new DefaultBodyParser();

    private final Map<String, HttpRequestBodyParser> bodyParsers;

    public HttpRequestBodyParsers(Map<String, HttpRequestBodyParser> bodyParsers) {
        this.bodyParsers = bodyParsers;
    }

    public static HttpRequestBodyParsers createDefault(ObjectMapper jsonMapper) {
        return new HttpRequestBodyParsers(
                new ImmutableMap.Builder<String, HttpRequestBodyParser>()
                        .put("text/plain", new PlainTextBodyParser())
                        .put("application/json", new JsonBodyParser(jsonMapper))
                        .build());
    }

    public HttpRequestBodyParser getByContentType(@Nullable String contentType) {
        return Optional.ofNullable(contentType)
                .map(ct -> bodyParsers.get(HttpUtil.getMimeType(ct).toString()))
                .orElse(DEFAULT);
    }

    public static final class DefaultBodyParser implements HttpRequestBodyParser {
        @Override
        public Object parse(ByteBuf source, Charset charset, Class<?> clazz) {
            throw new UnsupportedMediaTypeException();
        }
    }
}
