package ru.maximvegorov.highloadcup.framework.results;

import java.util.Objects;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import ru.maximvegorov.highloadcup.framework.ActionResult;
import ru.maximvegorov.highloadcup.framework.HttpResponseBodyFormatter;

@ParametersAreNonnullByDefault
@Value
@AllArgsConstructor
public final class ObjectResult<T> implements ActionResult {
    @NonNull
    private final HttpResponseStatus status;
    @NonNull
    private final T result;

    @Override
    public FullHttpResponse execute(
            ByteBufAllocator byteBufAlloc, HttpResponseBodyFormatter bodyFormatter)
    {
        FullHttpResponse httpResponse = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                status,
                bodyFormatter.toByteBuf(byteBufAlloc, result));
        httpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, bodyFormatter.getContentType());
        return httpResponse;
    }
}
