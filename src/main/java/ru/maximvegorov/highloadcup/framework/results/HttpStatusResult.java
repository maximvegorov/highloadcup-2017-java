package ru.maximvegorov.highloadcup.framework.results;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import lombok.Value;
import ru.maximvegorov.highloadcup.framework.ActionResult;
import ru.maximvegorov.highloadcup.framework.HttpResponseBodyFormatter;
import ru.maximvegorov.highloadcup.framework.HttpResponses;

@ParametersAreNonnullByDefault
@Value
public final class HttpStatusResult implements ActionResult {
    private final HttpResponseStatus status;

    @Override
    public FullHttpResponse execute(ByteBufAllocator byteBufAlloc, HttpResponseBodyFormatter bodyFormatter) {
        return HttpResponses.createEmptyJsonResponse(status);
    }
}
