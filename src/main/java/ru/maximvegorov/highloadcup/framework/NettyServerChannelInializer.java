package ru.maximvegorov.highloadcup.framework;

import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpServerKeepAliveHandler;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import ru.maximvegorov.highloadcup.framework.handlers.RequestMappingHandler;
import ru.maximvegorov.highloadcup.framework.handlers.UnhandledErrorHandler;

@ParametersAreNonnullByDefault
public final class NettyServerChannelInializer extends ChannelInitializer<Channel> {
    private final LoggingHandler loggingHandler;
    private final RequestMappingHandler requestMappingHandler;
    private final UnhandledErrorHandler unhandledErrorHandler;

    private final int readTimeoutInMs;
    private final int writeTimeoutInMs;
    private final int maxMessageSize;

    public NettyServerChannelInializer(
            RequestMappingHandler requestMappingHandler,
            int readTimeoutInMs,
            int writeTimeoutInMs,
            int maxMessageSize)
    {
        this.loggingHandler = new LoggingHandler(LogLevel.DEBUG);
        this.requestMappingHandler = requestMappingHandler;
        this.readTimeoutInMs = readTimeoutInMs;
        this.writeTimeoutInMs = writeTimeoutInMs;
        this.maxMessageSize = maxMessageSize;
        this.unhandledErrorHandler = new UnhandledErrorHandler();
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline()
                .addLast(loggingHandler)
                .addLast(new ReadTimeoutHandler(readTimeoutInMs, TimeUnit.MILLISECONDS))
                .addLast(new WriteTimeoutHandler(writeTimeoutInMs, TimeUnit.MILLISECONDS))
                .addLast(new HttpServerCodec())
                .addLast(new HttpServerKeepAliveHandler())
                .addLast(new HttpObjectAggregator(maxMessageSize))
                .addLast(requestMappingHandler)
                .addLast(unhandledErrorHandler);
    }
}
