package ru.maximvegorov.highloadcup.framework;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.collect.ImmutableList;
import io.netty.handler.codec.http.HttpMethod;

@ParametersAreNonnullByDefault
public final class RouteTable {
    private final List<Route> routes;

    RouteTable(List<Route> routes) {
        this.routes = ImmutableList.copyOf(routes);
    }

    public Optional<RouteMatchResult> anyMatch(HttpMethod method, @Nullable String path) {
        return routes.stream()
                .map(r -> r.matches(method, path))
                .filter(Objects::nonNull)
                .findFirst();
    }
}
