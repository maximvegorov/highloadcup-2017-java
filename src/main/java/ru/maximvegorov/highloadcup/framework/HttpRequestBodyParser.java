package ru.maximvegorov.highloadcup.framework;

import java.nio.charset.Charset;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBuf;

@FunctionalInterface
@ParametersAreNonnullByDefault
public interface HttpRequestBodyParser {
    Object parse(ByteBuf source, Charset charset, Class<?> clazz);
}
