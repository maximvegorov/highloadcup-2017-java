package ru.maximvegorov.highloadcup.framework.handlers;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@ChannelHandler.Sharable
@Slf4j
public class UnhandledErrorHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("Unhandled exception", cause);
        ctx.close().addListener(f -> {
            if (!f.isSuccess()) {
                log.error("Can't close socket", f.cause());
            }
        });
    }
}
