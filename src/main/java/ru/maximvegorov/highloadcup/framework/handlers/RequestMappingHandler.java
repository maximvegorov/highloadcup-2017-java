package ru.maximvegorov.highloadcup.framework.handlers;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.QueryStringDecoder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import ru.maximvegorov.highloadcup.framework.HttpRequestBodyParsers;
import ru.maximvegorov.highloadcup.framework.HttpRequestHandlerInvoker;
import ru.maximvegorov.highloadcup.framework.HttpResponseBodyFormatters;
import ru.maximvegorov.highloadcup.framework.HttpResponses;
import ru.maximvegorov.highloadcup.framework.RouteTable;
import ru.maximvegorov.highloadcup.framework.exceptions.HttpStatusErrorException;

import static ru.maximvegorov.highloadcup.framework.ActionResults.notFound;

@ChannelHandler.Sharable
@ParametersAreNonnullByDefault
@Slf4j
public final class RequestMappingHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    @NonNull
    private final ExecutorService workerThreadPool;
    @NonNull
    private final RouteTable routeTable;
    @NonNull
    private final HttpRequestBodyParsers bodyParsers;
    @NonNull
    private final HttpResponseBodyFormatters bodyFormatters;

    public RequestMappingHandler(
            ExecutorService workerThreadPool,
            RouteTable routeTable,
            HttpRequestBodyParsers bodyParsers,
            HttpResponseBodyFormatters bodyFormatters)
    {
        this.workerThreadPool = workerThreadPool;
        this.routeTable = routeTable;
        this.bodyParsers = bodyParsers;
        this.bodyFormatters = bodyFormatters;
    }

    private static FullHttpResponse handleException(Throwable e) {
        log.error("Can't process request", e);
        if (e instanceof HttpStatusErrorException) {
            return HttpResponses.createEmptyJsonResponse(((HttpStatusErrorException) e).getStatus());
        } else {
            return HttpResponses.createEmptyJsonResponse(HttpResponseStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private CompletableFuture<FullHttpResponse> processRequest(ChannelHandlerContext ctx, FullHttpRequest msg) {
        try {
            val method = msg.method();

            val decoder = new QueryStringDecoder(msg.uri());

            val contentType = msg.headers().get(HttpHeaderNames.CONTENT_TYPE);

            val bodyParser = bodyParsers.getByContentType(contentType);

            val bodyFormatter = bodyFormatters.getByAccept(msg.headers().get(HttpHeaderNames.ACCEPT));

            Function<Class<?>, Object> contentProvider = clazz -> bodyParser.parse(
                    msg.content(),
                    HttpUtil.getCharset(contentType, StandardCharsets.UTF_8),
                    clazz);

            return routeTable.anyMatch(method, decoder.path())
                    .map(mr -> CompletableFuture.supplyAsync(
                            () -> HttpRequestHandlerInvoker.invoke(
                                    mr,
                                    method,
                                    decoder.parameters(),
                                    contentProvider),
                            workerThreadPool))
                    .orElse(CompletableFuture.completedFuture(notFound()))
                    .thenApply(ar -> ar.execute(ctx.alloc(), bodyFormatter))
                    .exceptionally(RequestMappingHandler::handleException);
        } catch (RuntimeException e) {
            return CompletableFuture.completedFuture(handleException(e));
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        val keepAlive = HttpUtil.isKeepAlive(msg);

        msg.retain();

        processRequest(ctx, msg).thenAccept(response -> {
            msg.release();

            response.headers().setInt(
                    HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());

            if (!keepAlive) {
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            } else {
                response.headers().set(
                        HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
                ctx.writeAndFlush(response);
            }
        });
    }
}
