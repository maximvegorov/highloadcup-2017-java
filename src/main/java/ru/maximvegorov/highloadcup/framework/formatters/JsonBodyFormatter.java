package ru.maximvegorov.highloadcup.framework.formatters;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.util.AsciiString;
import lombok.NonNull;
import ru.maximvegorov.highloadcup.framework.AbstractHttpResponseBodyFormatter;

@ParametersAreNonnullByDefault
public final class JsonBodyFormatter extends AbstractHttpResponseBodyFormatter {
    @NonNull
    private final ObjectMapper jsonMapper;

    public JsonBodyFormatter(int priority, ObjectMapper jsonMapper) {
        super(priority, new AsciiString("application/json; charset=utf-8"));
        this.jsonMapper = jsonMapper;
    }

    @Override
    public ByteBuf toByteBuf(ByteBufAllocator byteBufAlloc, Object obj) {
        try {
            ByteBuf dest = byteBufAlloc.buffer();
            jsonMapper.writeValue(
                    new OutputStreamWriter(
                            new ByteBufOutputStream(dest), StandardCharsets.UTF_8),
                    obj);
            return dest;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
