package ru.maximvegorov.highloadcup.framework.formatters;

import java.nio.charset.StandardCharsets;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Utf8;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.AsciiString;
import ru.maximvegorov.highloadcup.framework.AbstractHttpResponseBodyFormatter;

@ParametersAreNonnullByDefault
public final class PlainTextBodyFormatter extends AbstractHttpResponseBodyFormatter {
    public PlainTextBodyFormatter(int priority) {
        super(priority, new AsciiString("text/plain; charset=utf-8"));
    }

    @Override
    public ByteBuf toByteBuf(ByteBufAllocator byteBufAlloc, Object obj) {
        ByteBuf dest = byteBufAlloc.buffer(Utf8.encodedLength(obj.toString()));
        dest.writeCharSequence(obj.toString(), StandardCharsets.UTF_8);
        return dest;
    }
}
