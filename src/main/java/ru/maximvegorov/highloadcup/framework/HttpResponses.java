package ru.maximvegorov.highloadcup.framework;

import java.nio.charset.StandardCharsets;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.AsciiString;

public final class HttpResponses {
    private static final ByteBuf EMPTY_JSON_OBJECT = Unpooled.unreleasableBuffer(
            Unpooled.copiedBuffer("{}", StandardCharsets.UTF_8));
    private static final AsciiString APPLICATION_JSON_UTF8 = AsciiString.of("application/json; charset=utf-8");

    public static FullHttpResponse createEmptyJsonResponse(HttpResponseStatus status) {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1, status, EMPTY_JSON_OBJECT.duplicate());
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, APPLICATION_JSON_UTF8);
        return response;
    }
}
