package ru.maximvegorov.highloadcup.framework;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.maximvegorov.highloadcup.framework.handlers.RequestMappingHandler;

@ParametersAreNonnullByDefault
@Slf4j
public final class NettyServer {
    @NonNull
    private final NettyServerConfig config;

    NettyServer(NettyServerConfig config) {
        this.config = config;
    }

    private static EpollEventLoopGroup createAcceptEventLoopGroup(int acceptThreadCount) {
        return new EpollEventLoopGroup(
                acceptThreadCount,
                new ThreadFactoryBuilder().setNameFormat("netty-server-accept-thread-%d").build());
    }

    private static EpollEventLoopGroup createIoEventLoopGroup(int ioThreadCount) {
        return new EpollEventLoopGroup(
                ioThreadCount,
                new ThreadFactoryBuilder().setNameFormat("netty-server-io-thread-%d").build());
    }

    private static ExecutorService createWorkerThreadPool(int workerThreadCount) {
        return Executors.newFixedThreadPool(
                workerThreadCount,
                new ThreadFactoryBuilder().setNameFormat("netty-server-worker-thread-%d").build());
    }

    private static Thread createShutdownHook(Channel serverChannel) {
        return new Thread("netty-server-shutdown-hook") {
            @Override
            public void run() {
                try {
                    serverChannel.close();
                } catch (RuntimeException e) {
                    log.error("Can't close server channel");
                }
            }
        };
    }

    private Channel createServerChannel(
            EpollEventLoopGroup acceptGroup,
            EpollEventLoopGroup ioGroup,
            ExecutorService workerThreadPool)
    {
        ServerBootstrap bootstrap = new ServerBootstrap()
                .option(ChannelOption.SO_BACKLOG, config.getBacklogSize())
                .channel(EpollServerSocketChannel.class)
                .group(acceptGroup, ioGroup)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(
                        new NettyServerChannelInializer(
                                new RequestMappingHandler(
                                        workerThreadPool, config.getRouteTable(),
                                        config.getBodyParsers(),
                                        config.getBodyFormatters()),
                                config.getReadTimeoutInMs(),
                                config.getWriteTimeoutInMs(),
                                config.getMaxMessageSize()));
        return bootstrap.bind(config.getPort()).syncUninterruptibly().channel();
    }

    public void run() {
        log.info("Starting...");
        try {
            EpollEventLoopGroup ioGroup = createIoEventLoopGroup(config.getIoThreadCount());
            try {
                ExecutorService workerThreadPool = createWorkerThreadPool(config.getWorkerThreadCount());
                try {
                    EpollEventLoopGroup acceptGroup = createAcceptEventLoopGroup(config.getAcceptThreadCount());
                    try {
                        Channel serverChannel = createServerChannel(acceptGroup, ioGroup, workerThreadPool);

                        Runtime.getRuntime().addShutdownHook(createShutdownHook(serverChannel));

                        log.info("Started");

                        try {
                            serverChannel.closeFuture().sync();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            log.info("Interrupted");
                        }

                        log.info("Shutting down...");
                    } finally {
                        acceptGroup.shutdownGracefully().awaitUninterruptibly();
                    }
                } finally {
                    MoreExecutors.shutdownAndAwaitTermination(
                            workerThreadPool, Integer.MAX_VALUE, TimeUnit.SECONDS);
                }
            } finally {
                ioGroup.shutdownGracefully().awaitUninterruptibly();
            }
            log.info("Shutting downed");
        } catch (RuntimeException e) {
            log.error("Can't start", e);
        }
    }
}
