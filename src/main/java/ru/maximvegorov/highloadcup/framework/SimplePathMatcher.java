package ru.maximvegorov.highloadcup.framework;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;

import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;

@ParametersAreNonnullByDefault
public final class SimplePathMatcher implements PathMatcher {
    private static final Splitter SEGMENT_SPLITTER = Splitter.on('/');

    private final List<Segment> segments;
    private final boolean hasAnyParamSegment;

    public SimplePathMatcher(String pathPattern) {
        if (!pathPattern.startsWith("/")) {
            throw new IllegalArgumentException("pathPatterm must start with '/'");
        }
        this.segments = Streams.stream(SEGMENT_SPLITTER.split(pathPattern.substring(1)))
                .map(s -> s.startsWith(":")
                        ? new ParamSegment(s.substring(1))
                        : new TextSegment(s))
                .collect(toList());
        this.hasAnyParamSegment = this.segments.stream().anyMatch(Segment::isParam);
    }

    private boolean hasAnyMismatch(String path) {
        int i = 0;
        int begin = 0;
        while (i < segments.size() && begin < path.length()) {
            begin++;
            int end = path.indexOf('/', begin);
            if (end == -1) {
                end = path.length();
            }
            if (!segments.get(i).matches(path, begin, end)) {
                return true;
            }
            i++;
            begin = end;
        }
        return i < segments.size() || begin < path.length();
    }

    private Map<String, String> extractParams(String path) {
        Map<String, String> params = new HashMap<>();

        int begin = 0;
        for (Segment segment : segments) {
            begin++;
            int end = path.indexOf('/', begin);
            if (end == -1) {
                end = path.length();
            }

            if (segment.isParam()) {
                params.put(segment.getParamName(), path.substring(begin, end));
            }

            begin = end;
        }

        return params;
    }

    @Override
    public Map<String, String> matches(String path) {
        if (!path.startsWith("/")) {
            throw new IllegalArgumentException("path must start with '/'");
        }

        if (hasAnyMismatch(path)) {
            return null;
        }

        if (!hasAnyParamSegment) {
            return emptyMap();
        }

        return extractParams(path);
    }

    private static abstract class Segment {
        private boolean param;

        abstract boolean matches(String path, int begin, int end);

        abstract boolean isParam();

        String getParamName() {
            throw new UnsupportedOperationException();
        }
    }

    private static final class TextSegment extends Segment {
        private final String text;

        private TextSegment(String text) {
            this.text = text;
        }

        @Override
        boolean matches(String path, int begin, int end) {
            return path.regionMatches(begin, text, 0, text.length());
        }

        @Override
        boolean isParam() {
            return false;
        }
    }

    private static final class ParamSegment extends Segment {
        private final String paramName;

        private ParamSegment(String paramName) {
            if (paramName.isEmpty()) {
                throw new IllegalArgumentException("paramName is empty");
            }
            this.paramName = paramName;
        }

        @Override
        boolean matches(String path, int begin, int end) {
            return true;
        }

        @Override
        boolean isParam() {
            return true;
        }

        @Override
        String getParamName() {
            return paramName;
        }
    }
}
