package ru.maximvegorov.highloadcup.framework;

import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.handler.codec.http.HttpMethod;
import lombok.Getter;
import lombok.ToString;

@ParametersAreNonnullByDefault
@Getter
@ToString(exclude = {"pathMatcher", "handler"})
public final class Route {
    private final Set<HttpMethod> methods;
    private final PathMatcher pathMatcher;
    private final HttpRequestHandler handler;
    private final Map<String, Object> routeParams;

    public Route(
            Set<HttpMethod> methods,
            PathMatcher pathMatcher,
            HttpRequestHandler handler,
            Map<String, Object> routeParams)
    {
        this.methods = methods;
        this.pathMatcher = pathMatcher;
        this.routeParams = routeParams;
        this.handler = handler;
    }

    RouteMatchResult matches(HttpMethod method, @Nullable String path) {
        if (!methods.contains(method)) {
            return null;
        }
        Map<String, String> urlParams = pathMatcher.matches(path);
        if (urlParams == null) {
            return null;
        }
        return new RouteMatchResult(this, urlParams);
    }
}
