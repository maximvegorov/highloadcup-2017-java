package ru.maximvegorov.highloadcup.framework;

import java.net.InetAddress;
import java.util.Objects;

import lombok.Builder;
import lombok.Getter;

@Getter
public final class NettyServerConfig {
    public static final int DEFAULT_ACCEPT_THREAD_COUNT = 1;
    public static final int DEFAULT_IO_THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    public static final int DEFAULT_WORKER_THREAD_COUNT = 2 * Runtime.getRuntime().availableProcessors();

    public static final int DEFAULT_BACKLOG_SIZE = 1024;

    public static final int DEFAULT_READ_TIMEOUT_IN_MS = 30 * 1000;
    public static final int DEFAULT_WRITE_TIMEOUT_IN_MS = 30 * 1000;

    public static final int DEFAULT_MAX_MESSAGE_SIZE = 256 * 1024;

    private final int acceptThreadCount;
    private final int ioThreadCount;
    private final int workerThreadCount;

    private final int backlogSize;

    private final InetAddress bindAddress;
    private final int port;

    private final int readTimeoutInMs;
    private final int writeTimeoutInMs;
    private final int maxMessageSize;

    private final RouteTable routeTable;
    private final HttpRequestBodyParsers bodyParsers;
    private final HttpResponseBodyFormatters bodyFormatters;

    @Builder
    private NettyServerConfig(
            int acceptThreadCount,
            int ioThreadCount,
            int workerThreadCount,
            int backlogSize,
            InetAddress bindAddress,
            int port,
            int readTimeoutInMs,
            int writeTimeoutInMs,
            int maxMessageSize, RouteTable routeTable,
            HttpRequestBodyParsers bodyParsers,
            HttpResponseBodyFormatters bodyFormatters)
    {
        if (acceptThreadCount <= 0) {
            throw new IllegalArgumentException("acceptThreadCount: " + acceptThreadCount);
        }
        if (ioThreadCount <= 0) {
            throw new IllegalArgumentException("ioThreadCount: " + ioThreadCount);
        }
        if (workerThreadCount <= 0) {
            throw new IllegalArgumentException("workerThreadCount: " + workerThreadCount);
        }

        if (backlogSize <= 1) {
            throw new IllegalArgumentException("backlogSize: " + backlogSize);
        }

        if (port <= 0 || port >= 65535) {
            throw new IllegalArgumentException("port: " + port);
        }

        if (readTimeoutInMs <= 0) {
            throw new IllegalArgumentException("readTimeoutInMs: " + readTimeoutInMs);
        }
        if (writeTimeoutInMs <= 0) {
            throw new IllegalArgumentException("writeTimeoutInMs: " + writeTimeoutInMs);
        }

        if (maxMessageSize <= 0) {
            throw new IllegalArgumentException("maxMessageSize: " + maxMessageSize);
        }

        Objects.requireNonNull(routeTable, "routeTable");
        Objects.requireNonNull(bodyParsers, "bodyParsers");
        Objects.requireNonNull(bodyParsers, "bodyFormatters");

        this.acceptThreadCount = acceptThreadCount;
        this.ioThreadCount = ioThreadCount;
        this.workerThreadCount = workerThreadCount;
        this.backlogSize = backlogSize;
        this.bindAddress = bindAddress;
        this.port = port;
        this.readTimeoutInMs = readTimeoutInMs;
        this.writeTimeoutInMs = writeTimeoutInMs;
        this.maxMessageSize = maxMessageSize;
        this.routeTable = routeTable;
        this.bodyParsers = bodyParsers;
        this.bodyFormatters = bodyFormatters;
    }

    public NettyServer createServer() {
        return new NettyServer(this);
    }

    public static final class NettyServerConfigBuilder {
        public int acceptThreadCount = DEFAULT_ACCEPT_THREAD_COUNT;
        public int ioThreadCount = DEFAULT_IO_THREAD_COUNT;
        public int workerThreadCount = DEFAULT_WORKER_THREAD_COUNT;

        public int backlogSize = DEFAULT_BACKLOG_SIZE;

        public int port = -1;

        public int readTimeoutInMs = DEFAULT_READ_TIMEOUT_IN_MS;
        public int writeTimeoutInMs = DEFAULT_WRITE_TIMEOUT_IN_MS;

        public int maxMessageSize = DEFAULT_MAX_MESSAGE_SIZE;
    }
}
