package ru.maximvegorov.highloadcup.framework;

import io.netty.handler.codec.http.HttpResponseStatus;
import ru.maximvegorov.highloadcup.framework.results.HttpStatusResult;
import ru.maximvegorov.highloadcup.framework.results.ObjectResult;

public class ActionResults {
    private static final ActionResult BAD_REQUEST = new HttpStatusResult(HttpResponseStatus.BAD_REQUEST);
    private static final ActionResult NOT_FOUND = new HttpStatusResult(HttpResponseStatus.NOT_FOUND);
    private static final ActionResult CREATED_OR_UPDATED = new HttpStatusResult(HttpResponseStatus.OK);

    public static ActionResult badRequest() {
        return BAD_REQUEST;
    }

    public static ActionResult notFound() {
        return NOT_FOUND;
    }

    public static ActionResult objectCreated() {
        return CREATED_OR_UPDATED;
    }

    public static ActionResult objectUpdated() {
        return CREATED_OR_UPDATED;
    }

    public static <T> ActionResult object(T obj) {
        return new ObjectResult<>(HttpResponseStatus.OK, obj);
    }

    public static <T> ActionResult object(T obj, HttpResponseStatus status) {
        return new ObjectResult<>(status, obj);
    }
}
