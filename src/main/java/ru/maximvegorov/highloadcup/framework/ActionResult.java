package ru.maximvegorov.highloadcup.framework;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.codec.http.FullHttpResponse;

@FunctionalInterface
@ParametersAreNonnullByDefault
public interface ActionResult {
    FullHttpResponse execute(
            ByteBufAllocator byteBufAlloc, HttpResponseBodyFormatter bodyFormatter);
}
