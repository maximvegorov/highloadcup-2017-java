package ru.maximvegorov.highloadcup.framework;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.handler.codec.http.HttpMethod;
import lombok.extern.slf4j.Slf4j;
import ru.maximvegorov.highloadcup.framework.exceptions.CantParseRequestContentException;

import static ru.maximvegorov.highloadcup.framework.ActionResults.badRequest;

@ParametersAreNonnullByDefault
@Slf4j
public final class HttpRequestHandlerInvoker {
    public static ActionResult invoke(
            RouteMatchResult matchResult,
            HttpMethod method,
            Map<String, List<String>> queryParams,
            Function<Class<?>, Object> bodyParser)
    {
        try {
            return matchResult.getRoute()
                    .getHandler()
                    .process(
                            new HttpRequestMessage(
                                    method,
                                    matchResult.getRoute().getRouteParams(),
                                    matchResult.getUrlParams(),
                                    queryParams,
                                    bodyParser));
        } catch (CantParseRequestContentException e) {
            log.debug("Can't parse request content", e);
            return badRequest();
        }
    }
}
