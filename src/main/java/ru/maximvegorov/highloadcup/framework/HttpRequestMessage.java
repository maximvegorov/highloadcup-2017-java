package ru.maximvegorov.highloadcup.framework;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.handler.codec.http.HttpMethod;

@ParametersAreNonnullByDefault
public final class HttpRequestMessage {
    private final HttpMethod httpMethod;
    private final Map<String, Object> routeParams;
    private final Map<String, String> urlParams;
    private final Map<String, List<String>> queryParams;
    private final Function<Class<?>, Object> bodyParser;

    HttpRequestMessage(
            HttpMethod httpMethod,
            Map<String, Object> routeParams,
            Map<String, String> urlParams,
            Map<String, List<String>> queryParams,
            Function<Class<?>, Object> bodyParser)
    {
        this.httpMethod = httpMethod;
        this.routeParams = routeParams;
        this.queryParams = queryParams;
        this.urlParams = urlParams;
        this.bodyParser = bodyParser;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public Map<String, Object> getRouteParams() {
        return routeParams;
    }

    public Map<String, String> getUrlParams() {
        return urlParams;
    }

    public Map<String, List<String>> getQueryParams() {
        return queryParams;
    }

    public <T> T getContent(Class<T> clazz) {
        return clazz.cast(bodyParser.apply(clazz));
    }
}
