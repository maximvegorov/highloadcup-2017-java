package ru.maximvegorov.highloadcup.framework.parsers;

import java.nio.charset.Charset;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBuf;
import ru.maximvegorov.highloadcup.framework.HttpRequestBodyParser;

@ParametersAreNonnullByDefault
public final class PlainTextBodyParser implements HttpRequestBodyParser {
    @Override
    public Object parse(ByteBuf source, Charset charset, Class<?> clazz) {
        if (clazz != String.class) {
            throw new IllegalArgumentException("clazz: " + clazz);
        }
        return source.toString(charset);
    }
}
