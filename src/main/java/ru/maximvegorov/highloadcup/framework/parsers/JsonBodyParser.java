package ru.maximvegorov.highloadcup.framework.parsers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;

import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import lombok.NonNull;
import ru.maximvegorov.highloadcup.framework.HttpRequestBodyParser;
import ru.maximvegorov.highloadcup.framework.exceptions.CantParseRequestContentException;

@ParametersAreNonnullByDefault
public final class JsonBodyParser implements HttpRequestBodyParser {
    @NonNull
    private final ObjectMapper jsonMapper;

    public JsonBodyParser(ObjectMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }

    @Override
    public Object parse(ByteBuf source, Charset charset, Class<?> clazz) {
        try {
            return jsonMapper.readValue(
                    new InputStreamReader(
                            new ByteBufInputStream(source), charset), clazz);
        } catch (JsonProcessingException e) {
            throw new CantParseRequestContentException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
