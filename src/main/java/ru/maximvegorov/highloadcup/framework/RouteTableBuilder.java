package ru.maximvegorov.highloadcup.framework;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.handler.codec.http.HttpMethod;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;

@ParametersAreNonnullByDefault
public final class RouteTableBuilder {
    private final List<Route> routes = new ArrayList<>();

    public RouteTableBuilder get(String pathPattern, HttpRequestHandler handler) {
        append(new Route(
                singleton(HttpMethod.GET),
                new SimplePathMatcher(pathPattern),
                handler,
                emptyMap()));
        return this;
    }

    public RouteTableBuilder post(String pathPattern, HttpRequestHandler handler) {
        append(new Route(
                singleton(HttpMethod.POST),
                new SimplePathMatcher(pathPattern),
                handler,
                emptyMap()));
        return this;
    }

    public RouteTableBuilder put(String pathPattern, HttpRequestHandler handler) {
        append(new Route(
                singleton(HttpMethod.PUT),
                new SimplePathMatcher(pathPattern),
                handler,
                emptyMap()));
        return this;
    }

    public RouteTableBuilder delete(String pathPattern, HttpRequestHandler handler) {
        append(new Route(
                singleton(HttpMethod.DELETE),
                new SimplePathMatcher(pathPattern),
                handler,
                emptyMap()));
        return this;
    }

    public RouteTableBuilder append(Route route) {
        routes.add(route);
        return this;
    }

    public RouteTable build() {
        return new RouteTable(routes);
    }
}
