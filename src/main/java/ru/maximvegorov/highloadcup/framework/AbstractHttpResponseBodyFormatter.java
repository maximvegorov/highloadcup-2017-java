package ru.maximvegorov.highloadcup.framework;

import io.netty.util.AsciiString;

public abstract class AbstractHttpResponseBodyFormatter implements HttpResponseBodyFormatter {
    private final int priority;
    private final AsciiString contentType;

    protected AbstractHttpResponseBodyFormatter(int priority, AsciiString contentType) {
        this.priority = priority;
        this.contentType = contentType;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public AsciiString getContentType() {
        return contentType;
    }
}
