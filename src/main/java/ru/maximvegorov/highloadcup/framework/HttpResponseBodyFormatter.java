package ru.maximvegorov.highloadcup.framework;

import javax.annotation.ParametersAreNonnullByDefault;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.AsciiString;

@ParametersAreNonnullByDefault
public interface HttpResponseBodyFormatter {
    int getPriority();

    AsciiString getContentType();

    ByteBuf toByteBuf(ByteBufAllocator byteBufAlloc, Object obj);
}
