package ru.maximvegorov.highloadcup.framework;

import java.util.Map;

@FunctionalInterface
public interface PathMatcher {
    Map<String, String> matches(String path);
}
