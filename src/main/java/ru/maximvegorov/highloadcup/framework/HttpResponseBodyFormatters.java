package ru.maximvegorov.highloadcup.framework;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Streams;
import org.apache.commons.lang3.tuple.Pair;
import ru.maximvegorov.highloadcup.framework.exceptions.UnsupportedMediaTypeException;
import ru.maximvegorov.highloadcup.framework.formatters.JsonBodyFormatter;
import ru.maximvegorov.highloadcup.framework.formatters.PlainTextBodyFormatter;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@ParametersAreNonnullByDefault
public final class HttpResponseBodyFormatters {
    private final static Splitter ACCEPT_SPLITTER = Splitter.on(',')
            .trimResults()
            .omitEmptyStrings();

    private final Map<String, SubTypeFormatters> typeFormatters;
    private final HttpResponseBodyFormatter highestPriorityFormatter;

    public HttpResponseBodyFormatters(Map<String, HttpResponseBodyFormatter> bodyFormatters) {
        Objects.requireNonNull(bodyFormatters, "bodyFormatters");
        if (bodyFormatters.size() == 0) {
            throw new IllegalArgumentException("bodyFormatters.size() == 0");
        }

        this.typeFormatters = bodyFormatters.keySet()
                .stream()
                .map(mt -> Pair.of(mt, mt.split("/")))
                .collect(
                        groupingBy(
                                p -> p.getRight()[0],
                                toMap(p -> p.getRight()[1], p -> bodyFormatters.get(p.getLeft()))))
                .entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey, e -> new SubTypeFormatters(e.getValue())));

        this.highestPriorityFormatter = bodyFormatters.values().stream()
                .max(Comparator.comparingInt(HttpResponseBodyFormatter::getPriority))
                .orElseThrow(AssertionError::new);
    }

    public static HttpResponseBodyFormatters createDefault(ObjectMapper jsonMapper) {
        return new HttpResponseBodyFormatters(
                new ImmutableMap.Builder<String, HttpResponseBodyFormatter>()
                        .put("text/plain", new PlainTextBodyFormatter(1))
                        .put("application/json", new JsonBodyFormatter(2, jsonMapper))
                        .build());
    }

    public HttpResponseBodyFormatter getByAccept(String accept) {
        try {
            List<AcceptMimeType> mimeTypes = Streams.stream(ACCEPT_SPLITTER.split(accept))
                    .map(AcceptMimeType::parse)
                    .sorted(Comparator.comparingInt(AcceptMimeType::getQ))
                    .collect(toList());

            if (mimeTypes.isEmpty()) {
                throw new UnsupportedMediaTypeException();
            }

            for (AcceptMimeType mimeType : mimeTypes) {
                if (mimeType.getType().equals("*")) {
                    return highestPriorityFormatter;
                }

                SubTypeFormatters subTypeFormatters = typeFormatters.get(mimeType.getType());
                if (subTypeFormatters == null) {
                    throw new UnsupportedMediaTypeException();
                }

                if (mimeType.getSubType().equals("*")) {
                    return subTypeFormatters.getHighestPriorityFormatter();
                }

                HttpResponseBodyFormatter bodyFormatter = subTypeFormatters.getBySubType(mimeType.getSubType());
                if (bodyFormatter == null) {
                    throw new UnsupportedMediaTypeException();
                }

                return bodyFormatter;
            }

            throw new UnsupportedMediaTypeException();
        } catch (RuntimeException e) {
            throw new UnsupportedMediaTypeException(e);
        }
    }

    private static final class SubTypeFormatters {
        private final Map<String, HttpResponseBodyFormatter> bodyFormatters;
        private final HttpResponseBodyFormatter highestPriorityFormatter;

        private SubTypeFormatters(Map<String, HttpResponseBodyFormatter> bodyFormatters) {
            this.bodyFormatters = bodyFormatters;
            this.highestPriorityFormatter = bodyFormatters.values()
                    .stream()
                    .max(Comparator.comparingInt(HttpResponseBodyFormatter::getPriority))
                    .orElseThrow(AssertionError::new);
        }

        HttpResponseBodyFormatter getBySubType(String subType) {
            return bodyFormatters.get(subType);
        }

        HttpResponseBodyFormatter getHighestPriorityFormatter() {
            return highestPriorityFormatter;
        }
    }
}
