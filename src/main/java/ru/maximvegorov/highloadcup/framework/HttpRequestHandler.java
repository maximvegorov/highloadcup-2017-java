package ru.maximvegorov.highloadcup.framework;

@FunctionalInterface
public interface HttpRequestHandler {
    ActionResult process(HttpRequestMessage msg);
}
