package ru.maximvegorov.highloadcup.framework;

import java.util.Map;

import javax.annotation.ParametersAreNonnullByDefault;

import lombok.Getter;
import lombok.ToString;

@ParametersAreNonnullByDefault
@Getter
@ToString
public final class RouteMatchResult {
    private final Route route;
    private final Map<String, String> urlParams;

    RouteMatchResult(Route route, Map<String, String> urlParams) {
        this.route = route;
        this.urlParams = urlParams;
    }
}
