package ru.maximvegorov.highloadcup.resources;

import javax.inject.Inject;

import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.DatabaseService;
import ru.maximvegorov.highloadcup.db.rows.VisitRow;
import ru.maximvegorov.highloadcup.mappers.VisitMapper;
import ru.maximvegorov.highloadcup.models.VisitDto;

import static ru.maximvegorov.highloadcup.models.VisitDto.MAX_MARK_VALUE;
import static ru.maximvegorov.highloadcup.models.VisitDto.MIN_MARK_VALUE;

public final class VisitsResource extends EntitiesWithIdResource<VisitDto, VisitRow, VisitMapper> {
    @Inject
    public VisitsResource(DatabaseService database, VisitMapper mapper) {
        super(database, mapper);
    }

    @Override
    protected AbstractTable<VisitRow> getTable() {
        return database.getVisitTable();
    }

    @Override
    protected boolean isValid(VisitDto visit, boolean isCreate) {
        if ((isCreate || visit.getUserId() != null)
                && (visit.getUserId() == null || !database.getUserTable().contains(visit.getUserId())))
        {
            return false;
        }

        if ((isCreate || visit.getLocationId() != null)
                && (visit.getLocationId() == null || !database.getLocationTable().contains(visit.getLocationId())))
        {
            return false;
        }

        if (isCreate && visit.getVisitedAt() == null) {
            return false;
        }

        return (!isCreate && visit.getMark() == null)
                || (visit.getMark() != null && visit.getMark() >= MIN_MARK_VALUE && visit.getMark() <= MAX_MARK_VALUE);
    }
}
