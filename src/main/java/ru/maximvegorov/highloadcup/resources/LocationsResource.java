package ru.maximvegorov.highloadcup.resources;

import javax.inject.Inject;

import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.DatabaseService;
import ru.maximvegorov.highloadcup.db.rows.LocationRow;
import ru.maximvegorov.highloadcup.framework.ActionResult;
import ru.maximvegorov.highloadcup.mappers.LocationMapper;
import ru.maximvegorov.highloadcup.models.LocationAvgMarkRequestDto;
import ru.maximvegorov.highloadcup.models.LocationAvgMarkResponseDto;
import ru.maximvegorov.highloadcup.models.LocationDto;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static ru.maximvegorov.highloadcup.framework.ActionResults.badRequest;
import static ru.maximvegorov.highloadcup.framework.ActionResults.notFound;
import static ru.maximvegorov.highloadcup.framework.ActionResults.object;

public final class LocationsResource extends EntitiesWithIdResource<LocationDto, LocationRow, LocationMapper> {
    private static final int MARK_AVG_PRECISION = 100000;

    @Inject
    public LocationsResource(DatabaseService database, LocationMapper mapper) {
        super(database, mapper);
    }

    @Override
    protected AbstractTable<LocationRow> getTable() {
        return database.getLocationTable();
    }

    @Override
    protected boolean isValid(LocationDto location, boolean isCreate) {
        if (isCreate && isEmpty(location.getPlace())) {
            return false;
        }

        if ((isCreate || location.getCountry() != null)
                && (isEmpty(location.getCountry()) || location.getCountry().length() > LocationRow.MAX_COUNTRY_SIZE))
        {
            return false;
        }

        if ((isCreate || location.getCity() != null)
                && (isEmpty(location.getCity()) || location.getCity().length() > LocationRow.MAX_CITY_SIZE))
        {
            return false;
        }

        return (!isCreate && location.getDistance() == null)
                || (location.getDistance() != null && location.getDistance() >= 0);
    }

    public ActionResult getAvgMark(Integer id, LocationAvgMarkRequestDto request) {
        if (request == null) {
            return badRequest();
        }

        if (id == null || !database.getLocationTable().contains(id)) {
            return notFound();
        }

        return object(
                LocationAvgMarkResponseDto.of(
                        database.getLocationAvgMark(
                                id,
                                request.getFromDate(),
                                request.getToDate(),
                                request.getFromAge(),
                                request.getToAge(),
                                request.getGender(),
                                MARK_AVG_PRECISION)));
    }
}
