package ru.maximvegorov.highloadcup.resources;

import javax.inject.Inject;

import lombok.val;
import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.DatabaseService;
import ru.maximvegorov.highloadcup.db.rows.UserRow;
import ru.maximvegorov.highloadcup.framework.ActionResult;
import ru.maximvegorov.highloadcup.mappers.UserMapper;
import ru.maximvegorov.highloadcup.models.UserDto;
import ru.maximvegorov.highloadcup.models.UserVisitsRequestDto;
import ru.maximvegorov.highloadcup.models.UserVisitsResponseDto;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static ru.maximvegorov.highloadcup.framework.ActionResults.badRequest;
import static ru.maximvegorov.highloadcup.framework.ActionResults.notFound;
import static ru.maximvegorov.highloadcup.framework.ActionResults.object;

public final class UsersResource extends EntitiesWithIdResource<UserDto, UserRow, UserMapper> {
    @Inject
    public UsersResource(DatabaseService database, UserMapper mapper) {
        super(database, mapper);
    }

    @Override
    protected AbstractTable<UserRow> getTable() {
        return database.getUserTable();
    }

    @Override
    protected boolean isValid(UserDto user, boolean isCreate) {
        if ((isCreate || user.getEmail() != null)
                && (isEmpty(user.getEmail()) || user.getEmail().length() > UserRow.MAX_EMAIL_SIZE))
        {
            return false;
        }

        if ((isCreate || user.getFirstName() != null)
                && (isEmpty(user.getFirstName()) || user.getFirstName().length() > UserRow.MAX_NAME_SIZE))
        {
            return false;
        }

        if ((isCreate || user.getLastName() != null)
                && (isEmpty(user.getLastName()) || user.getLastName().length() > UserRow.MAX_NAME_SIZE))
        {
            return false;
        }

        if (isCreate && user.getGender() == null) {
            return false;
        }

        return !isCreate || user.getBirthdate() != null;
    }

    public ActionResult getVisits(Integer id, UserVisitsRequestDto request) {
        if (request == null) {
            return badRequest();
        }

        if (id == null || !database.getUserTable().contains(id)) {
            return notFound();
        }

        val visits = database.getUserVisits(
                id,
                request.getFromDate(),
                request.getToDate(),
                request.getCountry(),
                request.getToDistance());

        return object(
                UserVisitsResponseDto.of(
                        visits.stream().map(mapper::fromRow).collect(toList())));
    }
}
