package ru.maximvegorov.highloadcup.resources;

import java.util.Optional;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import ru.maximvegorov.highloadcup.db.AbstractTable;
import ru.maximvegorov.highloadcup.db.DatabaseService;
import ru.maximvegorov.highloadcup.db.rows.EntityWithIdRow;
import ru.maximvegorov.highloadcup.framework.ActionResult;
import ru.maximvegorov.highloadcup.framework.ActionResults;
import ru.maximvegorov.highloadcup.mappers.EntityMapper;
import ru.maximvegorov.highloadcup.models.EntityWithIdDto;

import static ru.maximvegorov.highloadcup.framework.ActionResults.badRequest;
import static ru.maximvegorov.highloadcup.framework.ActionResults.notFound;
import static ru.maximvegorov.highloadcup.framework.ActionResults.objectCreated;
import static ru.maximvegorov.highloadcup.framework.ActionResults.objectUpdated;

@RequiredArgsConstructor
public abstract class EntitiesWithIdResource<Dto extends EntityWithIdDto, Row extends EntityWithIdRow, Mapper extends EntityMapper<Dto, Row>> {
    @NonNull
    protected final DatabaseService database;
    @NonNull
    protected final Mapper mapper;

    protected abstract AbstractTable<Row> getTable();

    public ActionResult get(Integer entiryId) {
        return Optional.ofNullable(entiryId)
                .flatMap(id -> getTable().getById(id, mapper::fromRow))
                .map(ActionResults::object)
                .orElse(notFound());
    }

    protected boolean isValid(Dto dto, boolean isCreate) {
        return true;
    }

    public ActionResult create(Dto dto) {
        if (dto.getId() == null || !isValid(dto, true)) {
            return badRequest();
        }

        val inserted = getTable()
                .insert(dto.getId(), () -> mapper.toRow(dto));

        if (!inserted) {
            return badRequest();
        }

        return objectCreated();
    }

    public ActionResult update(Integer id, Dto dto) {
        if (id == null || dto.getId() != null) {
            return badRequest();
        }

        if (!getTable().getPrimaryKey().containsKey(id)) {
            return notFound();
        }

        if (!isValid(dto, false)) {
            return badRequest();
        }

        getTable().update(id, row -> mapper.updateRow(dto, row));

        return objectUpdated();
    }
}
