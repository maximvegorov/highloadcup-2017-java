package ru.maximvegorov.highloadcup.common.types;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public enum Gender {
    m,
    f;

    public static Map<String, Gender> MAP = ImmutableMap.<String, Gender>builder()
            .put("m", m)
            .put("f", f)
            .build();
}
