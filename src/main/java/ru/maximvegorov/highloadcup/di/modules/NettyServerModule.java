package ru.maximvegorov.highloadcup.di.modules;

import java.util.Optional;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import lombok.val;
import ru.maximvegorov.highloadcup.framework.HttpRequestBodyParsers;
import ru.maximvegorov.highloadcup.framework.HttpResponseBodyFormatters;
import ru.maximvegorov.highloadcup.framework.NettyServer;
import ru.maximvegorov.highloadcup.framework.NettyServerConfig;
import ru.maximvegorov.highloadcup.framework.RouteTable;

public class NettyServerModule extends AbstractModule {
    private static final String ACCEPT_THREAD_COUNT = "netty.server.acceptThreadCount";
    private static final String IO_THREAD_COUNT_KEY = "netty.server.ioThreadCount";
    private static final String WORKER_THREAD_SCALE_FACTOR_KEY = "netty.server.workerThreadScaleFactor";

    private static final String PORT_KEY = "netty.server.port";

    private static final int DEFAULT_ACCEPT_THREAD_COUNT = 1;
    private static final int DEFAULT_IO_THREAD_COUNT = 2;
    private static final int DEFAULT_WORKER_THREAD_SCALE_FACTOR = 2;

    private static final int DEFAULT_PORT = 8080;

    private static String getEnvKey(String propKey) {
        return propKey.toUpperCase().replace('.', '_');
    }

    private static Integer getIntProperty(String key, Integer defaultValue) {
        return Optional.ofNullable(System.getenv(getEnvKey(key)))
                .map(Optional::of)
                .orElse(Optional.ofNullable(System.getProperty(key)))
                .map(Integer::valueOf)
                .orElse(defaultValue);
    }

    private static int getAcceptThreadCount() {
        return getIntProperty(ACCEPT_THREAD_COUNT, DEFAULT_ACCEPT_THREAD_COUNT);
    }

    private static int getIoThreadCount() {
        return getIntProperty(IO_THREAD_COUNT_KEY, DEFAULT_IO_THREAD_COUNT);
    }

    private static int getWorkerThreadScaleFactor() {
        return getIntProperty(WORKER_THREAD_SCALE_FACTOR_KEY, DEFAULT_WORKER_THREAD_SCALE_FACTOR);
    }

    private static int getPort() {
        return getIntProperty(PORT_KEY, DEFAULT_PORT);
    }

    @Provides
    @Singleton
    public NettyServer nettyServer(
            RouteTable routeTable,
            HttpRequestBodyParsers bodyParsers,
            HttpResponseBodyFormatters bodyFormatters)
    {
        val availableProcessors = Runtime.getRuntime().availableProcessors();

        return NettyServerConfig.builder()
                .acceptThreadCount(getAcceptThreadCount())
                .ioThreadCount(Math.min(getIoThreadCount(), availableProcessors))
                .workerThreadCount(
                        availableProcessors * Math.max(1, getWorkerThreadScaleFactor()))
                .port(getPort())
                .routeTable(routeTable)
                .bodyParsers(bodyParsers)
                .bodyFormatters(bodyFormatters)
                .build()
                .createServer();
    }

    @Override
    protected void configure() {
        install(new InfrastructureModule());
        install(new RouteConfigModule());
    }
}
