package ru.maximvegorov.highloadcup.di.modules;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import lombok.val;
import org.mapstruct.factory.Mappers;
import ru.maximvegorov.highloadcup.db.DatabaseLoader;
import ru.maximvegorov.highloadcup.db.DatabaseService;
import ru.maximvegorov.highloadcup.mappers.LocationMapper;
import ru.maximvegorov.highloadcup.mappers.UserMapper;
import ru.maximvegorov.highloadcup.mappers.VisitMapper;
import ru.maximvegorov.highloadcup.resources.LocationsResource;
import ru.maximvegorov.highloadcup.resources.UsersResource;
import ru.maximvegorov.highloadcup.resources.VisitsResource;

public class ResourcesModule extends AbstractModule {
    private static final String DATA_ZIP_FILE_NAME_KEY = "data.zip.path";

    private static final String DEFAULT_DATA_ZIP_FILE_NAME = "/tmp/data/data.zip";

    @Provides
    @Singleton
    public DatabaseService databaseService() {
        val database = new DatabaseService();
        val dataZipFile = Paths.get(System.getProperty(DATA_ZIP_FILE_NAME_KEY, DEFAULT_DATA_ZIP_FILE_NAME));
        if (Files.exists(dataZipFile)) {
            new DatabaseLoader().loadData(database, dataZipFile);
        }
        return database;
    }

    @Override
    protected void configure() {
        bind(UserMapper.class).toInstance(Mappers.getMapper(UserMapper.class));
        bind(LocationMapper.class).toInstance(Mappers.getMapper(LocationMapper.class));
        bind(VisitMapper.class).toInstance(Mappers.getMapper(VisitMapper.class));

        bind(UsersResource.class).in(Singleton.class);
        bind(LocationsResource.class).in(Singleton.class);
        bind(VisitsResource.class).in(Singleton.class);
    }
}
