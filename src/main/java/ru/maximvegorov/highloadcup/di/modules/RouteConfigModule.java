package ru.maximvegorov.highloadcup.di.modules;

import java.util.Optional;
import java.util.function.Function;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import lombok.Value;
import lombok.val;
import ru.maximvegorov.highloadcup.common.types.Gender;
import ru.maximvegorov.highloadcup.framework.HttpRequestMessage;
import ru.maximvegorov.highloadcup.framework.RouteTable;
import ru.maximvegorov.highloadcup.framework.RouteTableBuilder;
import ru.maximvegorov.highloadcup.models.LocationAvgMarkRequestDto;
import ru.maximvegorov.highloadcup.models.LocationDto;
import ru.maximvegorov.highloadcup.models.UserDto;
import ru.maximvegorov.highloadcup.models.UserVisitsRequestDto;
import ru.maximvegorov.highloadcup.models.VisitDto;
import ru.maximvegorov.highloadcup.resources.LocationsResource;
import ru.maximvegorov.highloadcup.resources.UsersResource;
import ru.maximvegorov.highloadcup.resources.VisitsResource;

public class RouteConfigModule extends AbstractModule {
    private static Integer extractId(HttpRequestMessage request) {
        String id = request.getUrlParams().get("id");
        if (id == null) {
            return null;
        }
        return Ints.tryParse(id);
    }

    private static String extractQueryParamValue(
            HttpRequestMessage request, String paramName)
    {
        return Optional.ofNullable(request.getQueryParams().get(paramName))
                .map(l -> l.get(l.size() - 1))
                .orElse(null);
    }

    private static <T> Optional<QueryParamValue<T>> extractQueryParamValue(
            HttpRequestMessage request, String paramName, Function<String, T> parser)
    {
        return Optional.ofNullable(
                Optional.ofNullable(request.getQueryParams().get(paramName))
                        .map(l -> l.get(l.size() - 1))
                        .map(s -> QueryParamValue.of(parser.apply(s)))
                        .orElse(null));
    }

    private static <T> boolean isNotValidQueryParamValue(Optional<QueryParamValue<T>> paramValue) {
        return paramValue.isPresent() && !paramValue.get().isValid();
    }

    private static UserVisitsRequestDto extractUserVisitsRequest(HttpRequestMessage request) {
        val builder = UserVisitsRequestDto.builder();

        val fromDate = extractQueryParamValue(request, "fromDate", Longs::tryParse);
        if (isNotValidQueryParamValue(fromDate)) {
            return null;
        }
        builder.fromDate(fromDate.map(QueryParamValue::getValue).orElse(null));

        val toDate = extractQueryParamValue(request, "toDate", Longs::tryParse);
        if (isNotValidQueryParamValue(toDate)) {
            return null;
        }
        builder.toDate(toDate.map(QueryParamValue::getValue).orElse(null));

        builder.country(extractQueryParamValue(request, "country"));

        val toDistance = extractQueryParamValue(request, "toDistance", Ints::tryParse);
        if (isNotValidQueryParamValue(toDistance)) {
            return null;
        }
        builder.toDistance(toDistance.map(QueryParamValue::getValue).orElse(null));

        return builder.build();
    }

    private static LocationAvgMarkRequestDto extractLocationAvgMarkRequest(HttpRequestMessage request) {
        val builder = LocationAvgMarkRequestDto.builder();

        val fromDate = extractQueryParamValue(request, "fromDate", Longs::tryParse);
        if (isNotValidQueryParamValue(fromDate)) {
            return null;
        }
        builder.fromDate(fromDate.map(QueryParamValue::getValue).orElse(null));

        val toDate = extractQueryParamValue(request, "toDate", Longs::tryParse);
        if (isNotValidQueryParamValue(toDate)) {
            return null;
        }
        builder.toDate(toDate.map(QueryParamValue::getValue).orElse(null));

        val fromAge = extractQueryParamValue(request, "fromAge", Longs::tryParse);
        if (isNotValidQueryParamValue(fromAge)) {
            return null;
        }
        builder.fromAge(fromAge.map(QueryParamValue::getValue).orElse(null));

        val toAge = extractQueryParamValue(request, "toAge", Longs::tryParse);
        if (isNotValidQueryParamValue(toAge)) {
            return null;
        }
        builder.toAge(toAge.map(QueryParamValue::getValue).orElse(null));

        val gender = extractQueryParamValue(request, "gender", Gender.MAP::get);
        if (isNotValidQueryParamValue(gender)) {
            return null;
        }
        builder.gender(gender.map(QueryParamValue::getValue).orElse(null));

        return builder.build();
    }

    @Provides
    @Singleton
    public RouteTable routeTable(
            UsersResource usersResource,
            LocationsResource locationsResource,
            VisitsResource visitsResource)
    {
        return new RouteTableBuilder()
                // /users/*
                .get("/users/:id",
                        r -> usersResource.get(extractId(r)))
                .post("/users/new",
                        r -> usersResource.create(r.getContent(UserDto.class)))
                .post("/users/:id",
                        r -> usersResource.update(extractId(r), r.getContent(UserDto.class)))
                .get("/users/:id/visits",
                        r -> usersResource.getVisits(extractId(r), extractUserVisitsRequest(r)))

                // /locations/*
                .get("/locations/:id",
                        r -> locationsResource.get(extractId(r)))
                .post("/locations/new",
                        r -> locationsResource.create(r.getContent(LocationDto.class)))
                .post("/locations/:id",
                        r -> locationsResource.update(extractId(r), r.getContent(LocationDto.class)))
                .get("/locations/:id/avg",
                        r -> locationsResource.getAvgMark(extractId(r), extractLocationAvgMarkRequest(r)))

                // /visits/*
                .get("/visits/:id",
                        r -> visitsResource.get(extractId(r)))
                .post("/visits/new",
                        r -> visitsResource.create(r.getContent(VisitDto.class)))
                .post("/visits/:id",
                        r -> visitsResource.update(extractId(r), r.getContent(VisitDto.class)))

                .build();
    }

    @Override
    protected void configure() {
        install(new ResourcesModule());
    }

    @Value(staticConstructor = "of")
    private static class QueryParamValue<T> {
        private final T value;

        boolean isValid() {
            return value != null;
        }
    }
}
