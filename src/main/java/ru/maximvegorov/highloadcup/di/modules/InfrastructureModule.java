package ru.maximvegorov.highloadcup.di.modules;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import ru.maximvegorov.highloadcup.di.guice.ProvidesModule;
import ru.maximvegorov.highloadcup.framework.HttpRequestBodyParsers;
import ru.maximvegorov.highloadcup.framework.HttpResponseBodyFormatters;

public class InfrastructureModule extends ProvidesModule {
    @Provides
    @Singleton
    public ObjectMapper jsonMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.ACCEPT_FLOAT_AS_INT);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }

    @Provides
    @Singleton
    public HttpRequestBodyParsers bodyParsers(ObjectMapper jsonMapper) {
        return HttpRequestBodyParsers.createDefault(jsonMapper);
    }

    @Provides
    @Singleton
    public HttpResponseBodyFormatters bodyFormatters(ObjectMapper jsonMapper) {
        return HttpResponseBodyFormatters.createDefault(jsonMapper);
    }
}
