package ru.maximvegorov.highloadcup.di;

import com.google.inject.AbstractModule;
import ru.maximvegorov.highloadcup.di.modules.NettyServerModule;

public class GlobalModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new NettyServerModule());
    }
}
