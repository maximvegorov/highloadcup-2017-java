package ru.maximvegorov.highloadcup.models;

import lombok.Builder;
import lombok.Value;
import ru.maximvegorov.highloadcup.common.types.Gender;

@Value
@Builder
public final class LocationAvgMarkRequestDto {
    private final Long fromDate;
    private final Long toDate;
    private final Long fromAge;
    private final Long toAge;
    private final Gender gender;
}
