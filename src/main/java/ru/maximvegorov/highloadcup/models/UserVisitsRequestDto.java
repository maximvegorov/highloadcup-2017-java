package ru.maximvegorov.highloadcup.models;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public final class UserVisitsRequestDto {
    private final Long fromDate;
    private final Long toDate;
    private final String country;
    private final Integer toDistance;
}
