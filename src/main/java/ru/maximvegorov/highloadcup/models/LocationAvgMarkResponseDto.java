package ru.maximvegorov.highloadcup.models;

import lombok.Value;

@Value(staticConstructor = "of")
public final class LocationAvgMarkResponseDto {
    private final double avg;
}
