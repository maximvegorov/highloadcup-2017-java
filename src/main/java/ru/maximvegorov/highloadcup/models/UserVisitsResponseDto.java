package ru.maximvegorov.highloadcup.models;

import java.util.List;

import lombok.Value;

@Value(staticConstructor = "of")
public final class UserVisitsResponseDto {
    private final List<UserVisitDto> visits;
}
