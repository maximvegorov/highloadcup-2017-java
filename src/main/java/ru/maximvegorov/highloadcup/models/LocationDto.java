package ru.maximvegorov.highloadcup.models;

import lombok.Data;

@Data
public final class LocationDto implements EntityWithIdDto {
    private Integer id;
    private String place;
    private String country;
    private String city;
    private Integer distance;
}
