package ru.maximvegorov.highloadcup.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class UserVisitDto {
    private byte mark;
    @JsonProperty("visited_at")
    private long visitedAt;
    private String place;
}
