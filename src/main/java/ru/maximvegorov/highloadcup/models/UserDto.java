package ru.maximvegorov.highloadcup.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.maximvegorov.highloadcup.common.types.Gender;

@Data
public final class UserDto implements EntityWithIdDto {
    private Integer id;
    private String email;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private Gender gender;
    @JsonProperty("birth_date")
    private Long birthdate;
}
