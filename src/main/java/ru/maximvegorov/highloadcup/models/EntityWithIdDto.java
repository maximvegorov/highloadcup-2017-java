package ru.maximvegorov.highloadcup.models;

public interface EntityWithIdDto {
    Integer getId();
}
