package ru.maximvegorov.highloadcup.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class VisitDto implements EntityWithIdDto {
    public static final int MIN_MARK_VALUE = 0;
    public static final int MAX_MARK_VALUE = 5;

    private Integer id;
    @JsonProperty("location")
    private Integer locationId;
    @JsonProperty("user")
    private Integer userId;
    @JsonProperty("visited_at")
    private Long visitedAt;
    private Integer mark;
}
