package ru.maximvegorov.highloadcup;

import com.google.inject.Guice;
import com.google.inject.Injector;
import ru.maximvegorov.highloadcup.di.GlobalModule;
import ru.maximvegorov.highloadcup.framework.NettyServer;

public class Application {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new GlobalModule());
        injector.getInstance(NettyServer.class)
                .run();
    }
}
