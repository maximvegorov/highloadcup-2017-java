package ru.maximvegorov.highloadcup.mappers;

import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

public interface EntityMapper<Dto, Row> {
    Dto fromRow(Row row);

    Row toRow(Dto dto);

    @Mapping(target = "id", ignore = true)
    void updateRow(Dto dto, @MappingTarget Row row);
}
