package ru.maximvegorov.highloadcup.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import ru.maximvegorov.highloadcup.db.rows.LocationRow;
import ru.maximvegorov.highloadcup.models.LocationDto;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface LocationMapper extends EntityMapper<LocationDto, LocationRow> {
}
