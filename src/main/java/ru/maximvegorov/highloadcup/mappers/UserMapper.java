package ru.maximvegorov.highloadcup.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import ru.maximvegorov.highloadcup.db.rows.UserRow;
import ru.maximvegorov.highloadcup.db.rows.UserVisitRow;
import ru.maximvegorov.highloadcup.models.UserDto;
import ru.maximvegorov.highloadcup.models.UserVisitDto;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserMapper extends EntityMapper<UserDto, UserRow> {
    UserVisitDto fromRow(UserVisitRow row);
}
