package ru.maximvegorov.highloadcup.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import ru.maximvegorov.highloadcup.db.rows.VisitRow;
import ru.maximvegorov.highloadcup.models.VisitDto;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface VisitMapper extends EntityMapper<VisitDto, VisitRow> {
}
